﻿# IPL Data Project

This project is used to analyze to Indian Premier League dataset from 
https://www.kaggle.com/manasgarg/ipl.

It answers the following questions:
 1. Number of matches played per year of all the years in IPL.
 2. Matches won of all teams over all the years of IPL.
 3. Extra runs conceded for each team for a given year.
 4. The top economical bowlers of a particular year.
 5. Number of matches won by teams by batting first vs bowling first for each 
team throughout IPL.

## Environment
This project was developed in the follwing environment:
 - Python 3.7.3 [GCC 7.3.0] :: Anaconda, Inc. on linux[Ubuntu 18.04 LTS]
 - Postgresql-11

## File Structure
    ipl_data_project
    ├── configuration.ini
    ├── __init__.py
    ├── main.py
    ├── README.md
    └── requirements.txt
    ├── ├── datasets
    │   ├── deliveries.csv
    │   ├── invalid_data.csv
    │   ├── matches.csv
    │   ├── mock_deliveries.csv
    │   └── mock_matches.csv
    ├── ipl_analytics
    │   ├── csv_operations
    │   │   ├── economical_bowlers.py
    │   │   ├── extra_runs.py
    │   │   ├── __init__.py
    │   │   ├── matches_played_per_year.py
    │   │   ├── matches_won_by_batting_first.py
    │   │   └── stack_matches_won.py
    │   ├── psql_operations
    │   │   ├── get_sql_economical_bowlers.py
    │   │   ├── get_sql_extra_runs.py
    │   │   ├── get_sql_matches_played_per_year.py
    │   │   ├── get_sql_matches_won_batting_first.py
    │   │   ├── get_sql_matches_won_per_year.py
    │   │   ├── ___init__.py
    │   │   ├── sql_queries.py
    │   │   └── sql_queries.txt
    │   ├── helper_functions.py
    │   └── __init__.py
    ├── plots
    │   ├── economical_bowlers_2015.png
    │   ├── extra_runs_2016.png
    │   ├── matches_played_per_year.png
    │   ├── matches_won_batting_first.png
    │   └── stack_matches_won_per_year.png
    └── tests
        ├── __init__.py
        ├── test_economical_bowlers.py
        ├── test_extra_runs.py
        ├── test_for_invalid_inputs.py
        ├── test_matches_played_per_year.py
        ├── test_matches_won_by_batting_first.py
        └── test_matches_won_per_year.py

## Settings
Edit configuration.ini file to get the desired results on your choice of dataset

The congifuration.ini file has follwing sections:
    
    > DEFAULT
    > DATASETS
    > PLOT_PATHS
    > TEST_FILES
    > CSV_FUNCTIONS_TO_RUN
    > SQL_FUNCTIONS_TO_RUN
    > TEST_FUNCTIONS
    > DB_CONNECTION_SETTINGS
    > DB_TABLE_SETTINGS

**DEFAULT** 

Settings used throughout the project

    save_plots-------------------->Should save the plots?
    plot_for_sql------------------>Should plot for SQL queries?
    extra_runs_year--------------->The year to find out extra runs
    economical_bowlers_year------->The year to find out economical bowlers
    valid_bowler_over_cutoff------>The minimum overs to bowled to be a valid bowler
    top_bowler_ranking------------>The top 'x' numbers of bowlers you want to find
    bar_width--------------------->The bar width for bar chart

    test_extra_runs_year---------->The year to find out extra runs for tests
    test_economical_bowlers_year-->The year to find out economical bowlers for tests

**DATASETS**

Settings for csv files used for project

    datasets_filepath------------->The folder containing csv files for the project
    matches_filepath----------- -->The file containing matches data
    deliveries_filepath----------->The file containing deliveries data

**PLOT_PATHS**

Settings for storing the plots
    
    plot_folder------------------->The folder where you want to store your plots

**TEST_FILES**

Settings for tests in the project

    tests_directory--------------->The folder where unit tests are present


**CSV_FUNCTIONS_TO_RUN**

Settings for running the csv functions, each option is a flag to run the corresponding functions.

    run_csv_functions------------->set to 'true' to run csv functions, if 'false' no csv function would run

**SQL_FUNCTIONS_TO_RUN**

Settings for running the sql functions, each option is a flag to run the corresponding functions.

    run_sql_functions------------->set to 'true' to run sql functions, if 'false' no sql function would run

**TEST_FUNCTIONS**

Settings for running the test functions

    run_tests--------------------->set to 'true' to test functions, if 'false' no tests would run
    run_csv_tests----------------->set to 'true' to test csv functions
    run_sql_test------------------>set to 'true' to test sql funcitons

**DB_CONNECTION_SETTINGS**

Settings to connect to database

    dbname------------------------>name of database to connect to
    user-------------------------->user of database to connect to
    host-------------------------->host of database to connect to
    port-------------------------->port of database to connect to
    password---------------------->encrypted password of database to connect to
    secret------------------------>secret to decrypt the password(ask the maintainer)

**DB_TABLE_SETTINGS**

Settings to specify the table used in database

    matches_table_name------------>table containing matches data
    deliveries_table_name--------->table containing deliveries data
    test_matches_table_name------->table containing matches data used for tests
    test_deliveries_table_name---->table containing deliveries data used for tests

## Installing requirements
Run the following command to install the pip requirements for the project
>pip install -r requirements.txt

## Running  the project
Run main.py after saving your preferred settings.
>python3.7 main.py
