"""
Compute and plot the top 'TOP_BOWLER_RANKING' number of bowlers
according to their economies.
"""
from matplotlib import pyplot as plt

from ipl_analytics.helper_functions import get_matches_in_year, maximize_figure


def __get_valid_bowlers(**kwargs):
    """
    Get the valid bowlers data based on a cutoff limit.

    :key economy_of_bowlers: It's a dict in the format
                            {bowler:(runs given, balls bowled)}
    :key config: the ConfigParser object of the config_file
    :return: Returns the valid_bowlers filtered from the
            economy_of_bowlers
    :rtype: dict
    """
    economy_of_bowlers = kwargs.get('economy_of_bowlers')
    config = kwargs.get('config')
    valid_bowlers = {}
    for bowlers, runs_and_overs in economy_of_bowlers.items():
        if runs_and_overs[1] >=\
            config['DEFAULT'].getint('valid_bowler_over_cutoff'):
            valid_bowlers[bowlers] = runs_and_overs
    return valid_bowlers


def __get_economy_for_bowlers_per_over(**kwargs):
    """
    Calculate economy of bowlers per over from runs conceded and
    overs bowled.

    :key economy_of_bowlers: It's a dict in the format
                            {bowler:(runs given, overs bowled)}
    :return: Returns the economy per over for each bowler in the
            format {bowler: economy per over}
    :rtype: dict
    """
    economy_of_bowlers = kwargs.get('economy_of_bowlers')
    for bowler, economy in economy_of_bowlers.items():
        economy_of_bowlers[bowler] = economy[0] / economy[1]

    return economy_of_bowlers


def __get_economical_bowlers(**kwargs):
    """
    Get the top 'TOP_BOWLER_RANKING' numbers of bowlers according
    to their economies.

    :key deliveries: list of dicts where each dict corresponds
                    to a delivery
    :key matches_in_year: list of id's of matches in a season for
                         which the top economical bowlers should
                         be found
    :key config: the ConfigParser object of the config_file
    :return: dict in the format {bowler: economy per over} with
            len(dict) = TOP_BOWLER_RANKING
    :rtype: dict
    """
    deliveries = kwargs.get('deliveries')
    matches_in_year = kwargs.get('matches_in_year')
    config = kwargs.get('config')
    economy_of_bowlers = {}
    try:
        for delivery in deliveries:
            if int(delivery['is_super_over']):
                continue

            if delivery['match_id'] in matches_in_year:

                if delivery['bowler'] not in economy_of_bowlers:
                    economy_of_bowlers[delivery['bowler']] = [0, 0]

                if not int(delivery['extra_runs']):
                    economy_of_bowlers[delivery['bowler']][1] += 1
                    economy_of_bowlers[delivery['bowler']][0] += \
                        int(delivery['total_runs'])

                if int(delivery['extra_runs']):
                    if not(int(delivery['wide_runs']) or
                           int(delivery['noball_runs'])):
                        economy_of_bowlers[delivery['bowler']][1] += 1
                    economy_of_bowlers[delivery['bowler']][0] += \
                    int(delivery['total_runs']) - int(delivery['legbye_runs'])\
                        - int(delivery['bye_runs'])

        for bowler in economy_of_bowlers:
            balls = economy_of_bowlers[bowler][1]
            economy_of_bowlers[bowler][1] = (balls // 6) + \
                                            ((balls % 6) / 6)

        valid_bowlers = __get_valid_bowlers(
            config=config,
            economy_of_bowlers=economy_of_bowlers
        )

        economy_of_valid_bowlers = __get_economy_for_bowlers_per_over(
            economy_of_bowlers=valid_bowlers
        )

        economy_of_top_bowlers = \
            sorted(economy_of_valid_bowlers.items(),
                   key=lambda item: item[1])[:config['DEFAULT'].getint(
                       'top_bowler_ranking')]

        economy_of_top_bowlers = {
            bowler_economy[0]: bowler_economy[1] for bowler_economy in
            economy_of_top_bowlers}

        return economy_of_top_bowlers

    except KeyError as error:
        raise error

    except TypeError as error:
        raise error


def __plot_economical_bowlers(**kwargs):  # pragma: no cover
    """
    This function is used for plotting bar chart with economy on
    y-axis and names of bowlers on x-axis.

    :key year: The year for whihc top bowlers are displayed, used
                for displaying on plots and to save plots
    :key config: the ConfigParser object of the config_file
    :key economical_bowlers: dict in the format
                            {bowler: economy per over}
    :rtype: None
    """
    config = kwargs.get('config')
    year = kwargs.get('year')
    economical_bowlers = kwargs.get('economical_bowlers')

    bowler_names = list(economical_bowlers.keys())
    economies = list(economical_bowlers.values())

    plt.bar(bowler_names, economies)
    plt.xlabel('Bowlers')
    plt.ylabel('Economy')
    plt.xticks(rotation=30)
    plt.title('Top {} bowlers for the year {}'.format(
        config['DEFAULT']['top_bowler_ranking'], year))

    if config['DEFAULT'].getboolean('save_plots'):
        plt.savefig(config['PLOT_PATHS']['economical_bowlers_filepath'] +\
            '_' + year, bbox_inches='tight')
    maximize_figure()
    plt.show()


def compute_and_plot_economical_bowlers(**kwargs):  # pragma: no cover
    """
    Compute and plot the top 'TOP_BOWLER_RANKING' number of bowlers
    according to their economies.

    :key matches: list of dicts where each dict is a match data
    :key deliveries: list of dicts where each dict is a delivery data
    :key year: integer corresponding to the year for which the top
                ecomical bowlers need to found
    :key config: the ConfigParser object of the config_file
    :rtype: None
    """
    config = kwargs.get('config')
    matches = kwargs.get('matches')
    deliveries = kwargs.get('deliveries')
    year = kwargs.get('year')

    matches_in_year = get_matches_in_year(
        matches=matches, year=year
    )

    economical_bowlers = __get_economical_bowlers(
        config=config, deliveries=deliveries, matches_in_year=matches_in_year
    )

    __plot_economical_bowlers(
        year=year, config=config,
        economical_bowlers=economical_bowlers
    )
