""" Compute and plot extra runs conceded for each team in a particular year."""
from matplotlib import pyplot as plt

from ipl_analytics.helper_functions import get_matches_in_year, \
    get_short_team_names


def __compute_extra_runs_conceded_per_team(**kwargs):
    """
    Get the extra runs for each team in a particular  year.
    
    :key deliveries: list of dicts where each dict is a delivery data
    :key matches_in_year: list of match_id's for the paricular year
    :return: dict in the format {team: extra_runs_conceded}
    :rtype: dict
    """
    deliveries = kwargs.get('deliveries')
    matches_in_year = kwargs.get('matches_in_year')
    extra_runs_per_team = {}
    try:
        for delivery in deliveries:
            if delivery['match_id'] in matches_in_year:
                if delivery['bowling_team'] not in extra_runs_per_team:
                    extra_runs_per_team[delivery['bowling_team']] = 0

                extra_runs_per_team[delivery['bowling_team']] += \
                    int(delivery['extra_runs'])

        return extra_runs_per_team

    except KeyError as error:
        raise error

    except TypeError as error:
        raise error


def __plot_extra_runs_conceded_per_team(**kwargs):  # pragma: no cover
    """
    Plot the extra runs for each team in a particular  year with runs
    on y-axis and teams on x-axis.

    :key extra_runs_per_team: dict in the format
                                {team: extra_runs_conceded}
    :key year:The year for whihc top bowlers are displayed, used
                for displaying on plots and to save plots
    :key config: the ConfigParser object of the config_file
    :rtype: None
    """
    config = kwargs.get('config')
    extra_runs_per_team = kwargs.get('extra_runs_per_team')
    year = kwargs.get('year')
    teams = extra_runs_per_team.keys()
    shorted_teams = get_short_team_names(teams=teams)
    teams = [shorted_teams[team] for team in extra_runs_per_team.keys()]
    extra_runs = list(extra_runs_per_team.values())

    plt.bar(teams, extra_runs)
    plt.xticks(teams, rotation=30)
    plt.title("Number of extras per team for year {}".format(year))
    plt.xlabel('Teams')
    plt.ylabel('Extra runs')

    if config['DEFAULT'].getboolean('save_plots'):
        plt.savefig(config['PLOT_PATHS']['extra_runs_filepath'] +\
            '_' + str(year))

    plt.show()


def compute_and_plot_extra_runs_conceded_per_team(**kwargs):  # pragma: no cover
    """
    Compute and plot extra runs conceded for each team in a particular year.

    :key matches: list of dicts where each dict is a match data
    :key deliveries: list of dicts where each dict is a delivery data
    :key year: integer corresponding to the year for which the top
                ecomical bowlers need to found
    :key config: the ConfigParser object of the config_file
    :rtype: None
    """
    config = kwargs.get('config')
    matches_in_year = get_matches_in_year(
        matches=kwargs.get('matches'),
        year=kwargs.get('year')
    )

    extra_runs_per_team = __compute_extra_runs_conceded_per_team(
        deliveries=kwargs.get('deliveries'),
        matches_in_year=matches_in_year
    )

    __plot_extra_runs_conceded_per_team(
        config=config,
        extra_runs_per_team=extra_runs_per_team,
        year=kwargs.get('year'),
    )
