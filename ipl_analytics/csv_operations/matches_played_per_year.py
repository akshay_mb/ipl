'''Compute and plot number matches played per year.'''
from matplotlib import pyplot as plt


def __matches_played_per_year(**kwargs):
    """
    Compute the number of matches played per year.

    :key matches: list of dicts where each dict is a match data
    :key config: the ConfigParser object of the config_file
    :return: dict of matches played per year in the format
            {year: number of matches}
    :rtype: dict
    """
    matches = kwargs.get('matches')
    matches_per_year = {}
    try:
        for match in matches:
            if match['season'] not in matches_per_year:
                matches_per_year[match['season']] = 0
            matches_per_year[match['season']] += 1

        return matches_per_year
    except KeyError as error:
        raise error
    except TypeError as error:
        raise error


def __plot_matches_played_per_year(**kwargs):  # pragma: no cover
    """
    Plot the number of matches played per year.

    :key matches_per_year: dict of matches played per year in the format
                            {year: numbe of matches}
    :key config: the ConfigParser object of the config_file
    """
    config = kwargs.get('config')
    matches_per_year = kwargs.get('matches_per_year')
    matches = sorted(matches_per_year.items())
    years = [match[0] for match in matches]
    number_of_matches = [i[1] for i in matches]

    plt.bar(years, number_of_matches)
    plt.title('Number of matches played per year')
    plt.xlabel('Years')
    plt.ylabel('No. of matches')

    if config['DEFAULT'].getboolean('save_plots'):
        plt.savefig(config['PLOT_PATHS']['matches_played_per_year_filepath'])
    plt.show()


def compute_and_plot_matches_played_per_year(**kwargs):  # pragma: no cover
    """
    Compute and plot the number of matches played per year.

    :key matches: list of dicts where each dict is a match data
    """
    matches = kwargs.get('matches')
    config = kwargs.get('config')
    matches_per_year = __matches_played_per_year(matches=matches)
    __plot_matches_played_per_year(
        config=config,
        matches_per_year=matches_per_year)
