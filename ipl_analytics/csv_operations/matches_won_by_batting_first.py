"""
Compute and plot the number of matches won by batting first vs bowling first.
"""
from matplotlib import pyplot as plt

from ipl_analytics.helper_functions import get_short_team_names


def __compute_matches_won_batting_first(**kwargs):
    """
    Compute the number of matches won by batting first vs bowling first
    first for each team.

    :key matches: list of dicts where each dict is a match data
    :return: dict of matches won by batting first vs bowling
            first for each team in the format
            {team: {bowling_first: number of matches won bowling first,
                    batting_first: number of matches won batting first}}
    :rtype: dict
    """
    matches = kwargs.get('matches')
    matches_won_batting_first = {}
    try:
        for match in matches:
            if match['result'] in ['tie', 'no result']:
                continue
            winning_team = match['winner']
            defending_team = match['team1']
            chasing_team = match['team2']

            if winning_team == 'Rising Pune Supergiant':
                winning_team = 'Rising Pune Supergiants'

            if defending_team == 'Rising Pune Supergiant':
                defending_team = 'Rising Pune Supergiants'

            if chasing_team == 'Rising Pune Supergiant':
                chasing_team = 'Rising Pune Supergiants'

            if winning_team not in matches_won_batting_first:
                matches_won_batting_first[winning_team] = {
                    'batting_first': 0,
                    'bowling_first': 0
                }

            if winning_team == defending_team:
                matches_won_batting_first[winning_team]['batting_first'] += 1

            if winning_team == chasing_team:
                matches_won_batting_first[winning_team]['bowling_first'] += 1

        return matches_won_batting_first
    except KeyError as error:
        raise error
    except TypeError as error:
        raise error


def __plot_matches_won_batting_first(**kwargs):  # pragma: no cover
    """
    Plot the number of matches won by batting first vs bowling first.

    :key matches_won_batting_first:dict of matches won by batting first vs
                    bowling first for each team in the format
                    {team: {bowling_first: number of matches won bowling first,
                    batting_first: number of matches won batting first}}
    :key config: the ConfigParser object of the config_file
    """
    config = kwargs.get('config')
    matches_won_batting_first = kwargs.get('matches_won_batting_first')
    teams = [team for team in matches_won_batting_first.keys()]
    bar_width = config['DEFAULT'].getfloat('bar_width')

    wins_batting_first = [
        matches_won_batting_first[team]['batting_first'] for team in teams
    ]

    wins_bowling_first = [
        matches_won_batting_first[team]['bowling_first'] for team in teams
    ]

    teams = [team for team in
             get_short_team_names(teams=teams).values()]

    label_locations_for_batting_wins = \
        [teams.index(team) - bar_width / 2 for team in teams]

    label_locations_for_bowling_wins = \
        [teams.index(team) + bar_width / 2 for team in teams]

    plt.bar(label_locations_for_batting_wins, wins_batting_first,
            bar_width, label='Batting First')
    plt.bar(label_locations_for_bowling_wins, wins_bowling_first,
            bar_width, label='Bowling First')
    plt.xlabel('Teams')
    plt.ylabel('Matches Won')
    plt.title('Matches won batting first vs bowling first')
    plt.xticks([loc for loc in range(len(teams))], teams)
    plt.legend()
    if config['DEFAULT'].getboolean('save_plots'):
        plt.savefig(config['PLOT_PATHS']['matches_won_batting_first_filepath'])
    plt.show()


def compute_and_plot_matches_won_batting_first(**kwargs):  # pragma: no cover
    """
    Compute and plot the number of matches won by batting first vs bowlingfirst
    
    :key matches: list of dicts where each dict is a match data
    :key config: the ConfigParser object of the config_file
    """
    config = kwargs.get('config')
    matches_won_batting_first = \
        __compute_matches_won_batting_first(matches=kwargs.get('matches'))

    __plot_matches_won_batting_first(
        config=config,
        matches_won_batting_first=matches_won_batting_first
    )
