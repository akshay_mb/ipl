"""
Compute and plot stacked bar chart of matches won of
all teams over all the years.
"""
from matplotlib import pyplot as plt

from ipl_analytics.helper_functions import maximize_figure


def __compute_matches_won_per_year_per_team(**kwargs):
    """
    Compute matches won per year for each team.

    :key matches: list of dicts where each dict is a match data
    :return: dict of matches won per year for each team in the format
            {team:[(year,number of matches won)]},
            list of years
    :rtype: dict, list
    """
    matches = kwargs.get('matches')
    matches_won_per_year_per_team = {}
    years = []
    try:
        for match in matches:
            team = match['winner']

            if team == '':
                team = 'No Result'

            if team == 'Rising Pune Supergiant':
                team = 'Rising Pune Supergiants'

            if team not in matches_won_per_year_per_team:
                matches_won_per_year_per_team[team] = {}

            if match['season'] not in matches_won_per_year_per_team[team]:
                matches_won_per_year_per_team[team][match['season']] = 0

            if match['season'] not in years:
                years.append(match['season'])

            matches_won_per_year_per_team[team][match['season']] += 1

        years.sort()

        for team in matches_won_per_year_per_team.items():
            for year in years:
                if year not in team[1]:
                    team[1][year] = 0
        for team in matches_won_per_year_per_team:
            matches_won_per_year_per_team[team] = \
                sorted(matches_won_per_year_per_team[team].items())

        return matches_won_per_year_per_team, years
    except KeyError as error:
        raise error
    except TypeError as error:
        raise error


def __plot_stack_matches_won(**kwargs):  # pragma: no cover
    """
    Plot matches won per year for each team.

    :key matches_won_per_year_per_team: dict of matches won per year for
    each team in the format {team:[(year,number of matches won)]}
    :key years: list of years
    :key config: the ConfigParser object of the config_file
    """
    config = kwargs.get('config')
    matches_won_per_year_per_team = kwargs.get(
        'matches_won_per_year_per_team')
    years = kwargs.get('years')
    previous_team_wins = [0] * len(years)
    for team in matches_won_per_year_per_team.items():
        wins = [win[1] for win in team[1]]
        plt.bar(years, wins, config['DEFAULT'].getfloat('bar_width'),
                bottom=previous_team_wins)
        previous_team_wins = [wins[i] + previous_team_wins[i]
                              for i in range(len(wins))]
    plt.xlabel('Years')
    plt.ylabel('No. of wins')
    plt.title('Number of matches won by teams per year', loc='left')
    legend_x, legend_y = 0.4, 1

    plt.legend(matches_won_per_year_per_team.keys(), framealpha=0.0,
               bbox_to_anchor=(legend_x, legend_y), ncol=4)

    maximize_figure()

    if config['DEFAULT'].getboolean('save_plots'):
        plt.savefig(config['PLOT_PATHS']['stack_matches_won_filepath'],
                    bbox_inches='tight')

    plt.show()


def compute_and_plot_stack_matches_won(**kwargs):  # pragma: no cover
    """
    Compute and plot matches won per year for each team

    :key matches: list of dicts where each dict is a match data
    :key config: the ConfigParser object of the config_file
    """
    matches = kwargs.get('matches')
    config = kwargs.get('config')
    matches_won_per_year_per_team, years = \
        __compute_matches_won_per_year_per_team(matches=matches)

    __plot_stack_matches_won(
        config=config,
        matches_won_per_year_per_team=matches_won_per_year_per_team,
        years=years)
