""" Contains helper functions that are to be called from across the project."""
import csv
import configparser
import base64
import matplotlib
from matplotlib import pyplot as plt
import psycopg2
import simplecrypt


def decrypt_password(salt, password):
    """
    Used for decrypting the password.

    :param salt: The secret key used to decrypt the password
    :type salt: str
    :param password: The encrypted password which needs to be decrypted
    :type password: str
    :return: decrypted password
    :rtype: str
    """  
    password = base64.b64decode(password)
    password = simplecrypt.decrypt(salt, password)

    return password.decode('utf-8')


def connect_to_database(host, port, database, user, password):
    """
    Connect to database.

    :param host: the host ip to connect to
    :type host: str
    :param port: the port to connect to
    :type port: str
    :param database: the database to connect to
    :type database: str
    :param user: the user to connect to
    :type user: str
    :param password: the password of the user to connect to
    :type password: str
    :return: the connection object
    :rtype: psycopg2.extensions.connection
    """
    connection = psycopg2.connect(
        host=host,
        port=port,
        database=database,
        user=user,
        password=password)

    return connection


def get_matches_in_year(**kwargs):
    """
    Function to get the id's of matches in a particular year.

    :key matches: list of dicts where each dict is a match data
    :key year: spcifies the condition for filtering the matches
                to the given year
    :return: list of id's of matches in the year
    :rtype: list
    """
    matches = kwargs.get('matches')
    year = kwargs.get('year')
    matches_in_year = []
    for match in matches:
        if match['season'] == year:
            matches_in_year.append(match['id'])

    return matches_in_year


def get_short_team_names(**kwargs):  # pragma: no cover
    """
    Function to shorten the teams names, useful in plots.

    :key teams: list of team names
    :return: list of shortend team names
    :rtype: list
    """
    teams = kwargs.get('teams')
    shorted_teams = {}
    for team in teams:
        if team not in shorted_teams:
            shorted_team = ''
            for char in team:
                if str.isupper(char):
                    shorted_team = shorted_team + char
            shorted_teams[team] = shorted_team

    return shorted_teams


def __extract_from_csv(**kwargs):
    """
    Function to extract data from csv and return list of dicts where
    each dict corresponds to a row in csv.

    :key csv_filepath: string corresponding to the location of csv
    :return: list of dicts where each dict corresponds to a row in csv
    :rtype: list
    """
    csv_data = []
    csv_filepath = kwargs.get('csv_filepath')

    try:
        with open(csv_filepath) as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for rows in csv_reader:
                csv_data.append(dict(rows))

        return csv_data

    except FileNotFoundError as error:
        raise error


def extract_matches(**kwargs):
    """
    Function to extract match data from a csv file if given or extracts
    from a default location.

    :key csv_filepath: string corresponding to the location of csv
    :return: list of dicts where each dict corresponds to a match
    :rtype: list
    """
    csv_filepath = kwargs.get('csv_filepath')
    matches = __extract_from_csv(csv_filepath=csv_filepath)

    return matches


def extract_deliveries(**kwargs):
    """
    Function to extract delivery data from a csv file if given or extracts
    from a default location.
    
    :key csv_filepath: string corresponding to the location of csv
    :return: list of dicts where each dict corresponds to a delivery
    :rtype: list
    """
    csv_filepath = kwargs.get('csv_filepath')
    deliveries = __extract_from_csv(csv_filepath=csv_filepath)

    return deliveries


def maximize_figure():  # pragma: no cover
    """
    Funtion to maximize the plot being displayed.
    """
    # Get the current figure manager and maximize it
    plot_backend = matplotlib.get_backend()
    mng = plt.get_current_fig_manager()
    if plot_backend == 'TkAgg':
        mng.resize(*mng.window.maxsize())
    elif plot_backend == 'wxAgg':
        mng.frame.Maximize(True)
    elif plot_backend in ('Qt5Agg', 'Qt4Agg'):
        mng.window.showMaximized()


def load_config(config_file='configuration.ini'):
    """
    Loads the mentioned config file and returns the ConfigParser object.

    :param config_file: the config file to load
    :type config_file: str
    :return: the ConfigParser object of the config_file
    :rtype: object configparser.ConfigParser
    """
    config = configparser.ConfigParser()
    config.read(config_file)

    return config
