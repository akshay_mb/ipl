"""Parse the sql result for top economical bowlers for a specific year
and plot it if mentioned."""
from ipl_analytics.csv_operations.economical_bowlers import\
    __plot_economical_bowlers


def parse_economical_bowlers(cursor, query, year, config):
    """
    This function takes the sql result for top economical bowlers for a specfic
    year and transforms it into the format specific for plotting it.

    :param cursor: Cursor object connected to required database
    :type cursor:object 'psycopg2.extensions.cursor'
    :param query: SQL query to execute
    :type query: str
    :param year: string representing the year for economical bowlers
    :type year: str
    :param config: the ConfigParser object of the config_file
    :type config: object configparser.ConfigParser
    :return: dict of top economical bowlers and their economies in the format
            {bowler: economy}
    :rtype: dict
    """
    cursor.execute(query)
    sql_economical_bowlers = cursor.fetchall()
    economy_of_bowlers = {}
    for bowler, economy in sql_economical_bowlers:
        economy_of_bowlers[bowler] = float(economy)
    if config['DEFAULT'].getboolean('plot_for_sql'):
        __plot_economical_bowlers(
            economical_bowlers=economy_of_bowlers,
            year=year, config=config
        )

    return economy_of_bowlers
