"""Parse the sql result for extra runs per team in a specific year
 and plot it if mentioned."""
from ipl_analytics.csv_operations.extra_runs import\
    __plot_extra_runs_conceded_per_team


def parse_extra_runs(cursor, query, config):
    """
    This function takes the sql result for extra runs conceded by each team
    for a specfic year and transforms it into the format specific for plotting.

    :param cursor: Cursor object connected to required database
    :type cursor:object 'psycopg2.extensions.cursor'
    :param query: SQL query to execute
    :type query: str
    :param config: the ConfigParser object of the config_file
    :type config: object configparser.ConfigParser
    :return: dict of matches won per year for each team in the format
            {team: extra runs conceded}
    :rtype: dict
    """
    cursor.execute(query)
    sql_extra_runs = cursor.fetchall()
    extra_runs_per_team = {}
    for team, extra_runs_conceded in sql_extra_runs:
        extra_runs_per_team[team] = extra_runs_conceded
    if config['DEFAULT'].getboolean('plot_for_sql'):
        __plot_extra_runs_conceded_per_team(
            extra_runs_per_team=extra_runs_per_team, config=config
        )

    return extra_runs_per_team
