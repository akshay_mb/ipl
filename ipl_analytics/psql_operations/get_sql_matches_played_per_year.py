'''Parse the sql result for matches per year and plot it if mentioned.'''
from ipl_analytics.csv_operations.matches_played_per_year import\
    __plot_matches_played_per_year


def parse_matches_played_per_year(cursor, query, config):
    """
    This function takes the sql result for matches per years and transforms
    it into the format specific for plotting it.

    :param cursor: Cursor object connected to required database
    :type cursor:object 'psycopg2.extensions.cursor'
    :param query: SQL query to execute
    :type query: str
    :param config: the ConfigParser object of the config_file
    :type config: object configparser.ConfigParser
    :return: Return the matches played per year in the format
             {year: number of matches}
    :rtype: dict
    """
    cursor.execute(query)
    sql_matches_played_per_year = cursor.fetchall()
    matches_played_per_year = {str(year): matches for year, matches in
                               sql_matches_played_per_year}
    if config['DEFAULT'].getboolean('plot_for_sql'):
        __plot_matches_played_per_year(
            matches_per_year=matches_played_per_year, config=config
        )

    return matches_played_per_year
