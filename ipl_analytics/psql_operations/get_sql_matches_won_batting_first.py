"""Parse the sql result for matches won by batting first vs bowling first
for each team and plot it if mentioned."""
from ipl_analytics.csv_operations.matches_won_by_batting_first import\
    __plot_matches_won_batting_first


def parse_matches_won_by_batting_first(cursor, query, config):
    """
    This function takes the sql result for matches won by batting first
    vs bowling first for each team and transforms it into the format specific
    for plotting it.

    :param cursor: Cursor object connected to required database
    :type cursor:object 'psycopg2.extensions.cursor'
    :param query: SQL query to execute
    :type query: str
    :param config: the ConfigParser object of the config_file
    :type config: object configparser.ConfigParser
    :return: dict of matches won per year for each team in the format
            {team: {bowling_first: number of matches won bowling first,
                    batting_first: number of matches won batting first}}
    :rtype: dict
    """
    cursor.execute(query)
    sql_matches_won_by_batting_first = cursor.fetchall()
    matches_won_batting_first = {}

    for team, wins_batting_first, wins_bowling_first in\
            sql_matches_won_by_batting_first:

        if team == 'Rising Pune Supergiant':
            team = 'Rising Pune Supergiants'
        if team is None:
            team = ''
        if team not in matches_won_batting_first:
            matches_won_batting_first[team] =\
                {'batting_first': wins_batting_first or 0,
                 'bowling_first': wins_bowling_first or 0}
        else:
            matches_won_batting_first[team]['batting_first'] +=\
                wins_batting_first or 0
            matches_won_batting_first[team]['bowling_first'] +=\
                wins_bowling_first or 0
    if config['DEFAULT'].getboolean('plot_for_sql'):
        __plot_matches_won_batting_first(
            matches_won_batting_first=matches_won_batting_first, config=config
        )

    return matches_won_batting_first
