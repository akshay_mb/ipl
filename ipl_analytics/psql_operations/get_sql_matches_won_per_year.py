'''Parse the sql result for matches won per year and plot it if mentioned.'''
from ipl_analytics.csv_operations.stack_matches_won import\
    __plot_stack_matches_won


def parse_matches_won_per_year(cursor, query, config):
    """
    This function takes the sql result for matches won by each team per year
    and transforms it into the format specific for plotting it.

    :param cursor: Cursor object connected to required database
    :type cursor:object 'psycopg2.extensions.cursor'
    :param query: SQL query to execute
    :type query: str
    :param config: the ConfigParser object of the config_file
    :type config: object configparser.ConfigParser
    :return: dict of matches won per year for each team in the format
            {team:[(year,number of matches won)]},
            list of years
    :rtype: dict, list
    """
    cursor.execute(query)
    sql_matches_won_per_year = cursor.fetchall()
    matches_won_per_year_per_team = {}
    years = []
    for team, year, wins in sql_matches_won_per_year:
        if team is None:
            team = 'No Result'

        if team == 'Rising Pune Supergiant':
            team = 'Rising Pune Supergiants'

        if team not in matches_won_per_year_per_team:
            matches_won_per_year_per_team[team] = {}

        if str(year) not in matches_won_per_year_per_team[team]:
            matches_won_per_year_per_team[team][str(year)] = 0

        if str(year) not in years:
            years.append(str(year))

        matches_won_per_year_per_team[team][str(year)] = wins

    years.sort()

    for team in matches_won_per_year_per_team.items():
        for year in years:
            if str(year) not in team[1]:
                team[1][str(year)] = 0
    for team in matches_won_per_year_per_team:
        matches_won_per_year_per_team[team] = \
            sorted(matches_won_per_year_per_team[team].items())
    if config['DEFAULT'].getboolean('plot_for_sql'):
        __plot_stack_matches_won(
            matches_won_per_year_per_team=matches_won_per_year_per_team,
            years=years, config=config
        )

    return matches_won_per_year_per_team, years
