"""Holds the SQL querry for each question."""

# 0--->delivery table
# 1--->matches table
# 2--->specific year for extra runs
EXTRA_RUNS = """SELECT
                    {0}.bowling_team,
                    SUM({0}.extra_runs)
                FROM {1}
                INNER JOIN {0} ON {1}.id = {0}.match_id
                WHERE
                    {1}.season = {2}
                GROUP BY
                    {0}.bowling_team;
                """


# 0--->delivery table
# 1--->matches table
# 2--->specific year for economical bowlers
# 3--->Limit for top 'x' bowlers
ECONOMICAL_BOWLERS = """WITH balls AS(
                    SELECT
                        {0}.bowler,
                        COUNT({0}.ball)
                    FROM {0}
                    INNER JOIN {1} ON {1}.id = {0}.match_id
                    WHERE
                        {1}.season = {2}
                        AND NOT {0}.is_super_over
                        AND {0}.wide_runs = 0
                        AND {0}.noball_runs = 0
                    GROUP BY
                        {0}.bowler
                        ),
                    runs AS(
                    SELECT
                        bowler,
                        SUM(total_runs - bye_runs - legbye_runs)
                    FROM {0}
                    INNER JOIN {1} ON {1}.id = {0}.match_id
                    WHERE
                        {1}.season = {2}
                        AND NOT {0}.is_super_over
                    GROUP BY
                        bowler
                        )
                    SELECT
                        balls.bowler,
                        (runs.sum /(balls.count / 6.0)) AS economy_per_over
                    FROM
                        balls
                        INNER JOIN runs ON runs.bowler = balls.bowler
                    GROUP BY
                        balls.bowler,
                        runs.sum,
                        balls.count
                    ORDER BY
                        economy_per_over
                    LIMIT
                        {3};"""

# 0--->matches table
MATCHES_PLAYED_PER_YEAR = """SELECT
                                season,
                                COUNT({0}.season)
                            FROM {0}
                            GROUP BY
                                {0}.season
                            ORDER BY
                                {0}.season;"""

# 0--->matches table
MATCHES_WON_PER_YEAR = """SELECT
                            {0}.winner AS teams,
                            {0}.season,
                            COUNT(*)
                        FROM {0}
                        GROUP BY
                            {0}.winner,
                            {0}.season
                        ORDER BY
                            {0}.winner,
                            {0}.season;"""

# 0--->matches table
MATCHES_WON_BY_BATTING_FIRST = """WITH
                                batting_first AS
                                (SELECT 
                                    winner, 
                                    COUNT(CASE WHEN winner=team1 THEN winner else NULL END) AS batting_first 
                                FROM {0}
                                WHERE 
                                    result NOT IN ('tie','no result')
                                GROUP BY 
                                    winner
                                    ),
                                bowling_first AS
                                (SELECT 
                                    winner, 
                                    COUNT(CASE WHEN winner=team2 THEN winner else NULL END) AS bowling_first 
                                FROM {0}
                                WHERE 
                                    result NOT IN ('tie','no result')
                                GROUP BY 
                                    winner
                                    )
                                SELECT 
                                    bowling_first.winner,
                                    batting_first,
                                    bowling_first
                                FROM 
                                    batting_first
                                RIGHT JOIN 
                                    bowling_first
                                ON 
                                    batting_first.winner=bowling_first.winner
                                GROUP BY 
                                    bowling_first.winner,
                                    batting_first,
                                    bowling_first;"""