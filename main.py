''' This is the entry point for this project. '''
import os
import pytest

from ipl_analytics.helper_functions import extract_deliveries,\
    extract_matches, load_config

from ipl_analytics.csv_operations.matches_played_per_year import \
    compute_and_plot_matches_played_per_year
from ipl_analytics.csv_operations.extra_runs import\
    compute_and_plot_extra_runs_conceded_per_team
from ipl_analytics.csv_operations.stack_matches_won import\
    compute_and_plot_stack_matches_won
from ipl_analytics.csv_operations.economical_bowlers import\
    compute_and_plot_economical_bowlers
from ipl_analytics.csv_operations.matches_won_by_batting_first import \
    compute_and_plot_matches_won_batting_first

from ipl_analytics.psql_operations import sql_queries
from ipl_analytics.psql_operations.get_sql_matches_played_per_year import\
    parse_matches_played_per_year
from ipl_analytics.psql_operations.get_sql_extra_runs import parse_extra_runs
from ipl_analytics.psql_operations.get_sql_economical_bowlers import\
    parse_economical_bowlers
from ipl_analytics.psql_operations.get_sql_matches_won_batting_first import\
    parse_matches_won_by_batting_first
from ipl_analytics.psql_operations.get_sql_matches_won_per_year import\
    parse_matches_won_per_year
from ipl_analytics.helper_functions import connect_to_database,\
    decrypt_password


def run_csv_functions(config):
    """
    Run the csv functions mentioned in config.

    :param config: the ConfigParser object of the config_file
    :type config: object configparser.ConfigParser
    """
    matches = extract_matches(
        csv_filepath=config['DATASETS']['matches_filepath'])

    deliveries = extract_deliveries(
        csv_filepath=config['DATASETS']['deliveries_filepath'])

    csv_functions_to_run = config['CSV_FUNCTIONS_TO_RUN']

    if csv_functions_to_run.getboolean('csv_matches_played_per_year'):
        compute_and_plot_matches_played_per_year(config=config, matches=matches)

    if csv_functions_to_run.getboolean('csv_stack_matches_won'):
        compute_and_plot_stack_matches_won(config=config, matches=matches)

    if csv_functions_to_run.getboolean('csv_extra_runs'):
        compute_and_plot_extra_runs_conceded_per_team(
            config=config, matches=matches, deliveries=deliveries,
            year=config['DEFAULT']['extra_runs_year']
        )

    if csv_functions_to_run.getboolean('csv_economical_bowlers'):
        compute_and_plot_economical_bowlers(
            config=config, matches=matches, deliveries=deliveries,
            year=config['DEFAULT']['economical_bowlers_year']
        )

    if csv_functions_to_run.getboolean('csv_matches_won_by_batting_first'):
        compute_and_plot_matches_won_batting_first(
            config=config, matches=matches
        )


def runs_sql_functions(config):
    """
    Run the sql functions mentioned in config.

    :param config: the ConfigParser object of the config_file
    :type config: object configparser.ConfigParser
    """
    connection = connect_to_database(
        host=config['DB_CONNECTION_SETTINGS']['host'],
        port=config['DB_CONNECTION_SETTINGS']['port'],
        database=config['DB_CONNECTION_SETTINGS']['dbname'],
        user=config['DB_CONNECTION_SETTINGS']['user'],
        password=decrypt_password(
            config['DB_CONNECTION_SETTINGS']['secret'],
            config['DB_CONNECTION_SETTINGS']['password']
        )
    )

    cursor = connection.cursor()
    matches_table = config['DB_TABLE_SETTINGS']['matches_table_name']
    deliveries_table = config['DB_TABLE_SETTINGS']['deliveries_table_name']

    if config['SQL_FUNCTIONS_TO_RUN'].getboolean('sql_matches_played_per_year'):
        parse_matches_played_per_year(
            cursor=cursor,
            query=sql_queries.MATCHES_PLAYED_PER_YEAR.format(matches_table),
            config=config
            )

    if config['SQL_FUNCTIONS_TO_RUN'].getboolean('sql_extra_runs'):
        parse_extra_runs(
            cursor=cursor,
            query=sql_queries.EXTRA_RUNS.format(
                deliveries_table, matches_table,
                config['DEFAULT']['extra_runs_year']
                ),
            config=config
        )

    if config['SQL_FUNCTIONS_TO_RUN'].getboolean('sql_economical_bowlers'):
        parse_economical_bowlers(
            cursor=cursor,
            query=sql_queries.ECONOMICAL_BOWLERS.format(
                deliveries_table, matches_table,
                config['DEFAULT']['economical_bowlers_year'],
                config['DEFAULT'].getint('top_bowler_ranking')),
            year=config['DEFAULT']['economical_bowlers_year'],
            config=config
        )

    if config['SQL_FUNCTIONS_TO_RUN'].getboolean('sql_matches_won_by_batting_first'):
        parse_matches_won_by_batting_first(
            cursor=cursor,
            query=sql_queries.MATCHES_WON_BY_BATTING_FIRST.format(matches_table),
            config=config
        )

    if config['SQL_FUNCTIONS_TO_RUN'].getboolean('sql_stack_matches_won'):
        parse_matches_won_per_year(
            cursor=cursor,
            query=sql_queries.MATCHES_WON_PER_YEAR.format(matches_table),
            config=config
        )

    cursor.close()
    connection.close()


def main(config):
    """
    This is the function which runs first on running the project
    It calls all the other functions according to the settings.

    :param config: the ConfigParser object of the config_file
    :type config: object configparser.ConfigParser
    """
    if config['DEFAULT']['save_plots']:
        os.makedirs(config['PLOT_PATHS']['plot_folder'], exist_ok=True)

    if config['CSV_FUNCTIONS_TO_RUN'].getboolean('run_csv_functions'):
        run_csv_functions(config)

    if config['SQL_FUNCTIONS_TO_RUN'].getboolean('run_sql_functions'):
        runs_sql_functions(config)

    if config['TEST_FUNCTIONS'].getboolean('run_tests'):
        pytest.main()


if __name__ == "__main__":
    CONFIG = load_config()
    main(CONFIG)
