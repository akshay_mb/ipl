from ipl_analytics.helper_functions import extract_matches, extract_deliveries,\
    get_matches_in_year, load_config
from ipl_analytics.helper_functions import connect_to_database,\
    decrypt_password
from ipl_analytics.psql_operations.get_sql_economical_bowlers import\
    parse_economical_bowlers
from ipl_analytics.psql_operations.sql_queries import ECONOMICAL_BOWLERS
from ipl_analytics.csv_operations.economical_bowlers import\
    __get_economical_bowlers


def test_economical_bowlers():
    expected_output = {'P Kumar': 1.0,
                       'AB Dinda': 4.0,
                       'I Sharma': 1.0,
                       'B Lee': 5.0,
                       'MS Gony': 7.0,
                       'JDP Oram': 9.0,
                       'S Sreesanth': 15.0,
                       'Z Khan': 18.0}
    test_config = load_config()
    if test_config['TEST_FUNCTIONS'].getboolean('run_csv_tests'):
        mock_matches = extract_matches(
            csv_filepath=test_config['DATASETS']['mock_matches_filepath']
        )
        mock_deliveries = extract_deliveries(
            csv_filepath=test_config['DATASETS']['mock_deliveries_filepath']
        )
        matches_in_year = get_matches_in_year(
            matches=mock_matches,
            year=test_config['DEFAULT']['test_economical_bowlers_year']
        )

        output = __get_economical_bowlers(
            matches=mock_matches, deliveries=mock_deliveries,
            matches_in_year=matches_in_year,
            config=test_config
        )
        assert output == expected_output

    if test_config['TEST_FUNCTIONS'].getboolean('run_sql_tests'):
        mock_connection = connect_to_database(
            host=test_config['DB_CONNECTION_SETTINGS']['host'],
            port=test_config['DB_CONNECTION_SETTINGS']['port'],
            database=test_config['DB_CONNECTION_SETTINGS']['dbname'],
            user=test_config['DB_CONNECTION_SETTINGS']['user'],
            password=decrypt_password(
                test_config['DB_CONNECTION_SETTINGS']['secret'],
                test_config['DB_CONNECTION_SETTINGS']['password'])
            )
        mock_cursor = mock_connection.cursor()

        mock_matches_table = test_config['DB_TABLE_SETTINGS']\
            ['test_matches_table_name']
        mock_deliveries_table = test_config['DB_TABLE_SETTINGS']\
            ['test_deliveries_table_name']

        sql_output = parse_economical_bowlers(
            cursor=mock_cursor,
            query=ECONOMICAL_BOWLERS.format(
                mock_deliveries_table,
                mock_matches_table,
                test_config['DEFAULT']['test_economical_bowlers_year'],
                test_config['DEFAULT']['top_bowler_ranking']),
            year=test_config['DEFAULT']['test_economical_bowlers_year'],
            config=test_config
        )
        assert sql_output == expected_output
