from ipl_analytics.helper_functions import extract_deliveries, extract_matches,\
    get_matches_in_year, load_config
from ipl_analytics.csv_operations.extra_runs import\
    __compute_extra_runs_conceded_per_team
from ipl_analytics.helper_functions import connect_to_database,\
    decrypt_password
from ipl_analytics.psql_operations.get_sql_extra_runs import parse_extra_runs
from ipl_analytics.psql_operations.sql_queries import EXTRA_RUNS


def test_extra_runs_per_team():
    expected_output = {'Rising Pune Supergiants': 7,
                       'Mumbai Indians': 0,
                       'Gujarat Lions': 0}
    test_config = load_config()
    if test_config['TEST_FUNCTIONS'].getboolean('run_csv_tests'):
        mock_matches = extract_matches(
            csv_filepath=test_config['DATASETS']['mock_matches_filepath']
        )
        mock_deliveries = extract_deliveries(
            csv_filepath=test_config['DATASETS']['mock_deliveries_filepath']
        )
        matches_in_year = get_matches_in_year(
            matches=mock_matches, year=test_config['DEFAULT']['test_extra_runs_year']
        )
        output = __compute_extra_runs_conceded_per_team(
            matches=mock_matches, deliveries=mock_deliveries,
            matches_in_year=matches_in_year
        )
        assert output == expected_output

    if test_config['TEST_FUNCTIONS'].getboolean('run_sql_tests'):
        mock_connection = connect_to_database(
            host=test_config['DB_CONNECTION_SETTINGS']['host'],
            port=test_config['DB_CONNECTION_SETTINGS']['port'],
            database=test_config['DB_CONNECTION_SETTINGS']['dbname'],
            user=test_config['DB_CONNECTION_SETTINGS']['user'],
            password=decrypt_password(
                test_config['DB_CONNECTION_SETTINGS']['secret'],
                test_config['DB_CONNECTION_SETTINGS']['password'])
        )
        mock_cursor = mock_connection.cursor()

        mock_matches_table = test_config['DB_TABLE_SETTINGS']\
            ['test_matches_table_name']
        mock_deliveries_table = test_config['DB_TABLE_SETTINGS']\
            ['test_deliveries_table_name']

        sql_output = parse_extra_runs(
            cursor=mock_cursor,
            query=EXTRA_RUNS.format(mock_deliveries_table, mock_matches_table,
                                    test_config['DEFAULT']['test_extra_runs_year']),
            config=test_config
        )
        assert sql_output == expected_output
