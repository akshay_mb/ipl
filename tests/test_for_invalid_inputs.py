import pytest

from ipl_analytics.helper_functions import extract_matches,\
    extract_deliveries, load_config
from ipl_analytics.csv_operations.economical_bowlers import\
    __get_economical_bowlers
from ipl_analytics.csv_operations.extra_runs import\
    __compute_extra_runs_conceded_per_team
from ipl_analytics.csv_operations.matches_played_per_year import\
    __matches_played_per_year
from ipl_analytics.csv_operations.matches_won_by_batting_first import\
    __compute_matches_won_batting_first
from ipl_analytics.csv_operations.stack_matches_won import\
    __compute_matches_won_per_year_per_team

def test_invalid_csv():
    test_config = load_config()
    matches = extract_matches(
        csv_filepath=test_config['DATASETS']['invalid_csv_filepath'])

    deliveries = extract_matches(
        csv_filepath=test_config['DATASETS']['invalid_csv_filepath'])

    with pytest.raises(KeyError):
        __get_economical_bowlers(deliveries=deliveries)
    with pytest.raises(KeyError):
        __compute_extra_runs_conceded_per_team(deliveries=deliveries)
    with pytest.raises(KeyError):
        __matches_played_per_year(matches=matches)
    with pytest.raises(KeyError):
        __compute_matches_won_batting_first(matches=matches)
    with pytest.raises(KeyError):
        __compute_matches_won_per_year_per_team(matches=matches)


def test_invalid_types():
    with pytest.raises(TypeError):
        __get_economical_bowlers(deliveries=5)
    with pytest.raises(TypeError):
        __compute_extra_runs_conceded_per_team(deliveries=5)
    with pytest.raises(TypeError):
        __matches_played_per_year(matches=5)
    with pytest.raises(TypeError):
        __compute_matches_won_batting_first(matches=5)
    with pytest.raises(TypeError):
        __compute_matches_won_per_year_per_team(matches=5)


def test_not_exist_csv():
    with pytest.raises(FileNotFoundError):
        extract_matches(csv_filepath='something.csv')
    with pytest.raises(FileNotFoundError):
        extract_deliveries(csv_filepath='something.csv')
