from ipl_analytics.helper_functions import extract_matches, load_config
from ipl_analytics.csv_operations.matches_played_per_year import\
    __matches_played_per_year
from ipl_analytics.helper_functions import connect_to_database,\
    decrypt_password
from ipl_analytics.psql_operations.get_sql_matches_played_per_year import\
    parse_matches_played_per_year
from ipl_analytics.psql_operations.sql_queries import MATCHES_PLAYED_PER_YEAR


def test_matches_per_year():
    expected_output = {'2008': 2, '2009': 2, '2010': 2, '2011': 4,
                       '2012': 5, '2013': 2, '2015': 5, '2016': 6, '2017': 2}
    test_config = load_config()
    if test_config['TEST_FUNCTIONS'].getboolean('run_csv_tests'):
        mock_matches = extract_matches(
            csv_filepath=test_config['DATASETS']['mock_matches_filepath']
        )
        output = __matches_played_per_year(matches=mock_matches)

        assert output == expected_output

    if test_config['TEST_FUNCTIONS'].getboolean('run_sql_tests'):
        mock_connection = connect_to_database(
            host=test_config['DB_CONNECTION_SETTINGS']['host'],
            port=test_config['DB_CONNECTION_SETTINGS']['port'],
            database=test_config['DB_CONNECTION_SETTINGS']['dbname'],
            user=test_config['DB_CONNECTION_SETTINGS']['user'],
            password=decrypt_password(
                test_config['DB_CONNECTION_SETTINGS']['secret'],
                test_config['DB_CONNECTION_SETTINGS']['password'])
        )
        mock_cursor = mock_connection.cursor()

        mock_matches_table = test_config['DB_TABLE_SETTINGS']\
            ['test_matches_table_name']

        sql_output = parse_matches_played_per_year(
            cursor=mock_cursor,
            query=MATCHES_PLAYED_PER_YEAR.format(mock_matches_table),
            config=test_config
        )
        assert sql_output == expected_output
