from ipl_analytics.helper_functions import extract_matches, load_config

from ipl_analytics.csv_operations.matches_won_by_batting_first import\
    __compute_matches_won_batting_first
from ipl_analytics.helper_functions import connect_to_database,\
    decrypt_password
from ipl_analytics.psql_operations.get_sql_matches_won_batting_first import\
    parse_matches_won_by_batting_first
from ipl_analytics.psql_operations.sql_queries import\
    MATCHES_WON_BY_BATTING_FIRST


def test_matches_won_by_batting_first():
    expected_output = {
        'Rising Pune Supergiants': {'batting_first': 0, 'bowling_first': 2},
        'Kolkata Knight Riders': {'batting_first': 2, 'bowling_first': 3},
        'Chennai Super Kings': {'batting_first': 3, 'bowling_first': 0},
        'Delhi Daredevils': {'batting_first': 0, 'bowling_first': 2},
        'Deccan Chargers': {'batting_first': 0, 'bowling_first': 1},
        'Mumbai Indians': {'batting_first': 1, 'bowling_first': 3},
        'Rajasthan Royals': {'batting_first': 2, 'bowling_first': 1},
        'Royal Challengers Bangalore': {'batting_first': 3, 'bowling_first': 2},
        'Pune Warriors': {'batting_first': 1, 'bowling_first': 0},
        'Gujarat Lions': {'batting_first': 0, 'bowling_first': 2},
        '': {'batting_first': 0, 'bowling_first': 0}
    }
    test_config = load_config()
    if test_config['TEST_FUNCTIONS'].getboolean('run_sql_tests'):
        mock_matches = extract_matches(
            csv_filepath=test_config['DATASETS']['mock_matches_filepath']
            )
        output = __compute_matches_won_batting_first(
            matches=mock_matches
        )
        assert output == expected_output

    if test_config['TEST_FUNCTIONS'].getboolean('run_sql_tests'):
        mock_connection = connect_to_database(
            host=test_config['DB_CONNECTION_SETTINGS']['host'],
            port=test_config['DB_CONNECTION_SETTINGS']['port'],
            database=test_config['DB_CONNECTION_SETTINGS']['dbname'],
            user=test_config['DB_CONNECTION_SETTINGS']['user'],
            password=decrypt_password(
                test_config['DB_CONNECTION_SETTINGS']['secret'],
                test_config['DB_CONNECTION_SETTINGS']['password'])
            )
        mock_cursor = mock_connection.cursor()

        mock_matches_table = test_config['DB_TABLE_SETTINGS']\
            ['test_matches_table_name']

        sql_output = parse_matches_won_by_batting_first(
            cursor=mock_cursor,
            query=MATCHES_WON_BY_BATTING_FIRST.format(mock_matches_table),
            config=test_config
        )
        assert sql_output == expected_output
