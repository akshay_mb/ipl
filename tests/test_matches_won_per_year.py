from ipl_analytics.csv_operations.stack_matches_won import\
    __compute_matches_won_per_year_per_team
from ipl_analytics.helper_functions import extract_matches, load_config
from ipl_analytics.helper_functions import connect_to_database,\
    decrypt_password
from ipl_analytics.psql_operations.get_sql_matches_won_per_year import\
    parse_matches_won_per_year
from ipl_analytics.psql_operations.sql_queries import MATCHES_WON_PER_YEAR

def test_matches_won_per_team_per_year():
    expected_output = (
        {
            'No Result': [('2008', 0), ('2009', 0), ('2010', 0), ('2011', 1),
                          ('2012', 0), ('2013', 0), ('2015', 0), ('2016', 0),
                          ('2017', 0)],
            'Sunrisers Hyderabad': [('2008', 0), ('2009', 0), ('2010', 0),
                                    ('2011', 0), ('2012', 0), ('2013', 0),
                                    ('2015', 0), ('2016', 0),
                                    ('2017', 1)],
            'Rising Pune Supergiants': [('2008', 0), ('2009', 0), ('2010', 0),
                                        ('2011', 0), ('2012', 0), ('2013', 0),
                                        ('2015', 0), ('2016', 1),
                                        ('2017', 1)],
            'Kolkata Knight Riders': [('2008', 1), ('2009', 0), ('2010', 1),
                                      ('2011', 0), ('2012', 0), ('2013', 1),
                                      ('2015', 1), ('2016', 1),
                                      ('2017', 0)],
            'Chennai Super Kings': [('2008', 1), ('2009', 0), ('2010', 0),
                                    ('2011', 0), ('2012', 0), ('2013', 0),
                                    ('2015', 2), ('2016', 0), ('2017', 0)],
            'Delhi Daredevils': [('2008', 0), ('2009', 1), ('2010', 0),
                                 ('2011', 0), ('2012', 1), ('2013', 0),
                                 ('2015', 0), ('2016', 0), ('2017', 0)],
            'Deccan Chargers': [('2008', 0), ('2009', 1), ('2010', 0),
                                ('2011', 0), ('2012', 0), ('2013', 0),
                                ('2015', 0), ('2016', 0), ('2017', 0)],
            'Mumbai Indians': [('2008', 0), ('2009', 0), ('2010', 1),
                               ('2011', 1), ('2012', 1), ('2013', 0),
                               ('2015', 0), ('2016', 1), ('2017', 0)],
            'Rajasthan Royals': [('2008', 0), ('2009', 0), ('2010', 0),
                                 ('2011', 1), ('2012', 1), ('2013', 0),
                                 ('2015', 1), ('2016', 0), ('2017', 0)],
            'Royal Challengers Bangalore': [('2008', 0), ('2009', 0),
                                            ('2010', 0), ('2011', 1),
                                            ('2012', 1), ('2013', 1),
                                            ('2015', 1), ('2016', 1),
                                            ('2017', 0)],
            'Pune Warriors': [('2008', 0), ('2009', 0), ('2010', 0),
                              ('2011', 0), ('2012', 1), ('2013', 0),
                              ('2015', 0), ('2016', 0), ('2017', 0)],
            'Gujarat Lions': [('2008', 0), ('2009', 0), ('2010', 0),
                              ('2011', 0), ('2012', 0), ('2013', 0),
                              ('2015', 0), ('2016', 2), ('2017', 0)]},
        ['2008', '2009', '2010', '2011', '2012', '2013', '2015', '2016',
         '2017'])
    test_config = load_config()
    if test_config['TEST_FUNCTIONS'].getboolean('run_sql_tests'):
        mock_matches = extract_matches(
            csv_filepath=test_config['DATASETS']['mock_matches_filepath']
            )
        output = __compute_matches_won_per_year_per_team(
            matches=mock_matches
        )

        assert output == expected_output

    if test_config['TEST_FUNCTIONS'].getboolean('run_sql_tests'):
        mock_connection = connect_to_database(
            host=test_config['DB_CONNECTION_SETTINGS']['host'],
            port=test_config['DB_CONNECTION_SETTINGS']['port'],
            database=test_config['DB_CONNECTION_SETTINGS']['dbname'],
            user=test_config['DB_CONNECTION_SETTINGS']['user'],
            password=decrypt_password(
                test_config['DB_CONNECTION_SETTINGS']['secret'],
                test_config['DB_CONNECTION_SETTINGS']['password'])
        )
        mock_cursor = mock_connection.cursor()

        mock_matches_table = test_config['DB_TABLE_SETTINGS']\
            ['test_matches_table_name']

        sql_output = parse_matches_won_per_year(
            cursor=mock_cursor,
            query=MATCHES_WON_PER_YEAR.format(mock_matches_table),
            config=test_config
        )
        assert sql_output == expected_output
